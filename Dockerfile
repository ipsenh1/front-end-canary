FROM node:15.11.0-alpine AS build
WORKDIR /frontend
COPY package*.json ./
RUN npm install --loglevel verbose
COPY . .
RUN npm run build -- --verbose

FROM nginx:alpine
WORKDIR /usr/share/nginx/html
RUN rm -rf ./*
COPY --from=build /frontend/dist/ipsenh .
EXPOSE 80
