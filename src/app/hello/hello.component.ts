import {Component, Input, OnInit} from '@angular/core';
import {HelloService} from './hello.service';
import {Observable} from 'rxjs';
import {Hello} from './hello';

@Component({
  selector: 'app-hello',
  templateUrl: './hello.component.html',
  providers: [HelloService],
  styleUrls: ['./hello.component.css']
})
export class HelloComponent implements OnInit {
  hello: Hello;

  constructor(private helloService: HelloService) {
  }

  ngOnInit(): void {
    this.getHello();
  }

  getHello(): void {
    this.helloService.getHello().subscribe(hello => (this.hello = hello));
  }
}
